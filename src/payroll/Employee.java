/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package payroll;

/**
 *
 * @author jayvy
 */
public class Employee {
    double wage;
    double hours;
    String name;
    public Employee(double wage, double hours, String name) {
        this.wage = wage;
        this.hours = hours;
        this.name = name;
    }

    double calculatePay(){
        return wage * hours;
    }
}

