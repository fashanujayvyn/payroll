/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package payroll;

/**
 *
 * @author jayvy
 */
public class EmployeeFactory {
    private static EmployeeFactory factory;
    
    private EmployeeFactory(){}
    
    public static EmployeeFactory getInstance(){
        
     if (factory==null)
         factory = new EmployeeFactory();
     return factory;
        
    }
    public static Employee getEmployee(EmployeeType type, double wage, double hours, String name, double bonus){
     switch(type){
         case MANAGER: return new Manager(wage,hours,name, bonus);
            case EMPLOYEE : return new Employee(wage,hours,name);
     }   
     
      
         
        
     return null;
    }
    
    
}
